# mail-server

Dockerized communication server 

Based on:

* tozd/postfix
* https://thomas-leister.de/en/mailserver-debian-stretch/
* https://nrdmnn.net/resources
* https://www.dev-eth0.de/2016/12/19/postfix_ldap/

- openldap - Users database
- postfix - SMTP part
- dovecot - IMAP part

## Default settings

* DOMAIN: (default: "example.tld") - domain
* MAIL_HOST: (default: "mail.example.tld") - server dns address
* ROOT_ALIAS: (default: "admin@example.tld") - alias for admin email

* LDAP_URL: (default: "ldap://ldap")
* LDAP_BASE_DN: (default: "dc=example,dc=tld")
* LDAP_READONLY_USERNAME: (default: "readonly")
* LDAP_READONLY_PASSWORD: (default: "password")

cn=admin,ou=people,dc=example,dc=tld # user which can admin / create other users
cn=readonly,ou=people,dc=example,dc=tld # user which can query mail addresses
postmaster@example.tld - postmaster_address is used as the From: header address in bounce mails

## Communication ports

* 25 SMTP: Unencrypted, used for sending emails between mail servers.
* 465 SMTPS: SSL, originally for SMTP over SSL for secure email submission (deprecated).
* 587 SMTP: STARTTLS, preferred port for sending emails from client to server.
* 143 IMAP: Unencrypted, used for accessing emails via IMAP.
* 993 IMAPS: SSL/TLS, used for securely accessing emails via IMAP.

## DNS Setup

TODO: check: https://thomas-leister.de/en/mailserver-debian-stretch/

## Build

```bash
docker build -t mail-server-test .
```

## Test

```bash
docker rm -f mail-server || true

docker run -d --name mail-server \
  -p 143:143 \
  -p 993:993 \
  --env MAIL_HOST="mail.example.tld" \
  --env DOMAIN="example.tld" \
  --env ROOT_ALIAS="admin@example.tld" \
  --env LDAP_URL="ldaps://ldap_host:636" \
  --env LDAP_READONLY_USERNAME="readonly" \
  --env LDAP_READONLY_PASSWORD="TO_FILL" \
  --env LDAP_BASE_DN="dc=example,dc=tld" \
  mail-server-test
```

Inside docker:

```bash
docker exec -it mail-server /bin/sh
```

Extract content from container:

```bash
rm -rf ./output/*

docker container cp mail-server:/etc/postfix ./output
docker container cp mail-server:/etc/dovecot ./output
docker container cp mail-server:/usr/share/dovecot ./output/share
docker container cp mail-server:/etc ./output
```

Reading logs

```bash
cat /tmp/*stderr*
```

### Test imap with cli

https://stackoverflow.com/questions/14959461/how-to-talk-to-imap-server-in-shell-via-openssl

```
openssl s_client -connect localhost:993
```

```
? LOGIN test@localhost passwordhere

? LIST "" "*"
? SELECT INBOX
```

## OpenLDAP

TODO: Describe how to setup openldap

```bash
docker network create -d overlay --attachable ldap

docker run --rm -it --name openldap \
  --network ldap \
  --env BITNAMI_DEBUG=true \
  --env LDAP_ADMIN_USERNAME=admin \
  --env LDAP_ADMIN_PASSWORD='{CRYPT}REST_OF_THE_HASH' \
  --env LDAP_USER_DC=people \
  --env LDAP_ROOT=dc=example,dc=tld \
  --user=200:0 \
  -v ./openldap_data:/bitnami/openldap \
  -v ./load-acl.sh:/docker-entrypoint-initdb.d/load-acl.sh \
  -v ./acl.ldif:/schema/acl.ldif \
  -v ./postfix.ldif:/schema/custom.ldif \
  bitnami/openldap:latest /bin/sh
```

### Testing

To check to see if the server is running and configured correctly, you can run a search against it with ldapsearch(1). By default, ldapsearch is installed as /usr/local/bin/ldapsearch:

```
ldapsearch -x -b '' -s base '(objectclass=*)' namingContexts
```

https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol

### Ldap search

https://devconnected.com/how-to-search-ldap-using-ldapsearch-examples/

```bash
ldapsearch -x -b <search_base> -H <ldap_host>
ldapsearch -x -b <search_base> -H <ldap_host> -D <bind_dn> -W
ldapsearch -x -b <search_base> -H <ldap_host> -D <bind_dn> -W "objectclass=*"
ldapsearch <previous_options> "(object_type)=(object_value)" <optional_attributes>
```


```bash
ADMIN_PASS="TO_FILL"
READONLY_PASS="TO_FILL"

#ldapsearch -H ldaps://ldap_host:636 -D cn=readonly,ou=people,dc=example,dc=tld -w "$READONLY_PASS" -x -b ou=people,dc=example,dc=tld "mailacceptinggeneralid=info@example.tld"
#ldapsearch -H ldaps://ldap_host:636 -D cn=admin,dc=example,dc=tld -w "$ADMIN_PASS" -x -b ou=people,dc=example,dc=tld "mailacceptinggeneralid=info@example.tld"
#ldapsearch -H ldaps://ldap_host:636 -D cn=admin,dc=example,dc=tld -w "$ADMIN_PASS" -x -b ou=people,dc=example,dc=tld "mailacceptinggeneralid"
#ldapsearch -H ldaps://ldap_host:636 -D cn=admin,dc=example,dc=tld -w "$ADMIN_PASS" -x -b dc=example,dc=tld "objectClass=*"

ldapsearch -H ldaps://ldap_host:636 -D cn=readonly,ou=people,dc=example,dc=tld -w "$READONLY_PASS" -x -b ou=domains,dc=example,dc=tld "(&(ObjectClass=dNSDomain)(associatedDomain=*))"
```

### Ldap modify

```bash
ADMIN_PASS="TO_FILL"
READONLY_PASS="TO_FILL"

ldap="dn: uid=jsmith4,ou=people,dc=example,dc=tld
changetype: add
cn: John Smith
mail: tttt@aazza.com
objectClass: inetOrgPerson
objectclass: postfixUser
sn: smith
maildrop: hhhh
"

echo "${ldap}" | /usr/bin/ldapmodify -H ldaps://ldap_host:636 -x -D cn=admin,dc=example,dc=tld -w "$ADMIN_PASS"

# This should fail:
echo "${ldap}" | /usr/bin/ldapmodify -H ldaps://ldap_host:636 -x -D cn=readonly,ou=people,dc=example,dc=tld -w "$READONLY_PASS"
```


### Initial config

Multiple domains

https://nrdmnn.net/resources/2-LDAP-managed-mail-server-with-Postfix-and-Dovecot-for-multiple-domains

https://serverfault.com/questions/828490/setting-up-multiple-domain-in-ldap-server

## Dovecot config

Modify /etc/dovecot/conf.d files.

Use `docker container cp mail-server:/etc/dovecot ./output` to extract those files.

## Postfix config

* main.cf: General configuration settings for Postfix.
* master.cf: Specific configurations for the Postfix processes/services.

Default file are located under:

* /etc/postfix/main.cf.proto
* /etc/postfix/master.cf.proto

Use `docker container cp mail-server:/etc/postfix ./output` to extract those files.

Copy them and modify or use `postconf` command.

### Testing postfix ldap conf

For inside of docker container run:

```bash
echo "Test message" | sendmail someemail@host.com
cat /var/log/postfix/postfix.log
```

Domains should be created under:

```bash
ls /var/mail/domains/
grep "Test message" /var/mail/domains/
```

For dovecot logs use (also grep config for debug_level = -1):

```bash
doveadm log errors
```
