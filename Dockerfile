FROM alpine
MAINTAINER Grzegorz Godlewski <gg@gitgis.com>

# Config

#COPY ./etc/aliases /etc/aliases
#COPY ./etc/hosts /etc/hosts

ENV DOMAIN "example.tld"
ENV MAIL_HOST "mail.example.tld"
ENV ROOT_ALIAS "admin@example.tld"

ENV LDAP_URL "ldap://ldap"
ENV LDAP_BASE_DN "dc=example,dc=tld"
ENV LDAP_READONLY_USERNAME "readonly"
ENV LDAP_READONLY_PASSWORD "password"

#####################################################
# BASE

RUN apk add --no-cache ca-certificates curl
VOLUME /var/log
VOLUME /etc/letsencrypt/live
RUN addgroup --system vmail --gid 20000
#RUN adduser vmail postfix

# Postfix
EXPOSE 25/tcp 465/tcp 587/tcp

VOLUME /var/log/postfix
VOLUME /var/spool/postfix

RUN apk add --no-cache postfix postfix-pcre postfix-ldap

COPY mail/etc/postfix /etc/postfix
COPY mail/etc/service /etc/service
COPY mail/etc/aliases /etc/aliases

#RUN chown -R postfix:postfix /etc/postfix
RUN chmod -R o-rwx /etc/postfix
RUN chown -R postfix /var/spool/postfix


RUN apk add --no-cache rspamd rspamd-controller rspamd-proxy rspamd-fuzzy
RUN mkdir /run/rspamd

VOLUME /var/lib/rspamd
VOLUME /var/log/rspamd

#####################################################
# Dovecot
EXPOSE 143/tcp 993/tcp
VOLUME /var/log/dovecot
VOLUME /var/mail

RUN apk add --no-cache dovecot dovecot-lmtpd dovecot-ldap dovecot-pigeonhole-plugin

COPY mail/etc/dovecot /etc/dovecot

#####################################################
# Supervisord

RUN apk add --no-cache supervisor

COPY ./etc/service /etc/service
COPY ./etc/supervisord.conf /etc/supervisord.conf

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
